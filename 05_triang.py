import pyglet
from math import sin, cos, pi
from time import time

window = pyglet.window.Window(width=1280, height=720)

@window.event
def on_draw():
    tam = 100
    ver = (640,360)
    raio = 300
    for i in range(tam):
        ang = (i / tam) * 2 * pi
        raio = (30 * sin(ang*6+time()))+ sin(time()/2)*300
        ver += (cos(ang)*raio + 640, sin(ang)*raio+360)
    ver += ver[2:4]
    
    cor = (0,0,0)
    for i in range(tam):
        ang = (i / tam) * 2 * pi
        cor += (int(sin(ang+time())*127)+127,
                int(sin(ang*2-time())*127)+127,
                int(cos(ang*2+time()*2)*127)+127)
    cor += cor[3:6]

    window.clear()
    pyglet.graphics.draw(tam+2, pyglet.graphics.GL_TRIANGLE_FAN,
        ('v2f',ver),
        ('c3B',cor))

@pyglet.clock.schedule
def update(dt):
    pass

pyglet.app.run()