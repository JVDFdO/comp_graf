import pyglet
from random import randint

window = pyglet.window.Window(width=1280, height=720)


@window.event
def on_draw():
    window.clear()

    size = 1000
    vertex = [0,] * (size * 2 * 3)
    for i in range(len(vertex)//2):
        vertex[i*2] = randint(0,1280)
        vertex[i*2+1] = randint(0,720)

    color = [0,] * (size * 3 * 3)
    for i in range(len(color)//9):
        c = (randint(0,255),randint(0,255),randint(0,255))
        for j in range(3):
            color[i*9+j*3] = c[0]
            color[i*9+j*3+1] = c[1]
            color[i*9+j*3+2] = c[2]
    
    pyglet.graphics.draw(size * 3, pyglet.graphics.GL_TRIANGLES, 
        ('v2i',vertex), ('c3B',color))
    
@pyglet.clock.schedule
def update(dt):
    pass

pyglet.app.run()