import pyglet
from random import randint
from math import sin, cos, pi, radians

size = 50
tempo = 0
tri = [randint(-100,100) for i in range(size * 2)]
tri2 = tri
cor = [randint(0,255) for i in range(size * 3)]
mat = pyglet.math.Mat3([  1.,  0.,  0.,
                          0.,  1.,  0.,
                          640.,  360.,  1.])


def transform2d(mat, ls):
    nls = []
    for i in range(len(ls)//2):
        vec = mat @ pyglet.math.Vec3(ls[i],ls[i+1],1)
        nls += [vec[0],vec[1]]
    return nls

def trans2d(x,y):
    return pyglet.math.Mat3([1.,0.,0.,0.,1.,0.,float(x),float(y),1.])

def scale2d(x,y):
    return pyglet.math.Mat3([float(x),0.,0.,0.,float(y),0.,0.,0.,1.])

def rotate2d(ang):
    rad = radians(ang)
    si = sin(rad)
    co = cos(rad)
    mat = [co, si, 0., -si, co, 0.,0.,0.,1.]
    return pyglet.math.Mat3(mat)


window = pyglet.window.Window(width=1280,height=720)

@window.event
def on_draw():
    window.clear()
    pyglet.graphics.draw(len(tri2)//2, pyglet.graphics.GL_TRIANGLES, 
            ('v2f',tri2),('c3B',cor))
        
@pyglet.clock.schedule
def update(dt):
    global tri, tri2, tempo
    tempo += dt
    transmat = scale2d(2,2)@ rotate2d(tempo*-250) @ trans2d(200,0) @ rotate2d(tempo*100) @ trans2d(640,360)
    tri2 = transform2d(transmat, tri)


pyglet.app.run()